from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from werkzeug.utils import secure_filename
from flask_migrate import Migrate
import os
import uuid
import csv

UPLOAD_FOLDER = "/path/to/the/uploads"
ALLOWED_EXTENSIONS = set(["txt", "csv"])

app = Flask(__name__)
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "mysql+pymysql://flaskdb:flaskdb@localhost/document_db_dev"
app.config["UPLOAD_FOLDER"] = "{}/uploads".format(os.getcwd())
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = 1
app.config["FLASK_DEBUG"] = 1

db = SQLAlchemy(app)
migrate = Migrate(app, db)


class Document(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    filename = db.Column(db.String(128))
    content_type = db.Column(db.String(128))
    original_filename = db.Column(db.String(128))
    file_path = db.Column(db.Text, nullable=False)

    def __repr__(self):
        return "<Document ID={} filename={}>".format(self.id, self.filename)


def allowed_file(filename):
    return (
        "." in filename
        and filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS
    )

@app.route("/")
def hello():
    return jsonify(dict(message="Hello World!"))

@app.route('/documents/<int:id>')
def document(id):
    file = Document.query.get(id).file_path

    with open(file) as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        rows = [row for row in csvReader]

    return jsonify(dict(data=rows))

@app.route("/documents", methods=["POST"])
def upload():
    # check if the post request has the file part
    if "file" not in request.files:
        return jsonify(dict(message="No file part")), 400
    file = request.files["file"]
    # if user does not select file, browser also
    # submit an empty part without filename
    if file.filename == "":
        return jsonify(dict(message="No selected file")), 400
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        temp_dir = str(uuid.uuid4())
        path = os.path.join(app.config["UPLOAD_FOLDER"], temp_dir)
        os.makedirs(path, exist_ok=True)
        file.save(os.path.join(path, filename))
        document = Document(
            filename=filename,
            original_filename=file.filename,
            content_type=file.mimetype,
            file_path=os.path.join(path, filename),
        )
        db.session.add(document)
        db.session.commit()
        target_path = os.path.join(app.config["UPLOAD_FOLDER"], str(document.id))
        os.makedirs(target_path, exist_ok=True)
        os.rename(os.path.join(path), os.path.join(target_path))
        document.file_path = os.path.join(target_path, filename)
        db.session.commit()
        return (
            jsonify(dict(message="Uploaded successfully {}".format(filename))),
            201,
        )

    return jsonify(dict(message="Upload failed")), 422
